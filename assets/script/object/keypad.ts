
import { _decorator, Component, Node } from 'cc';
import { KEYPAD_EVENT } from '../enum/keypad';
import { BaseButton } from './baseButton';
const { ccclass, property } = _decorator;

@ccclass('Keypad')
export class Keypad extends Component {
    @property(BaseButton)
    public readonly keypadUp?: BaseButton;

    @property(BaseButton)
    public readonly keypadRight?: BaseButton;

    @property(BaseButton)
    public readonly keypadDown?: BaseButton;

    @property(BaseButton)
    public readonly keypadLeft?: BaseButton;

    start() {
        this.keypadUp?.node.on(Node.EventType.TOUCH_START, () => {
            this.node.emit(KEYPAD_EVENT.PRESS_UP);
        });
        
        this.keypadRight?.node.on(Node.EventType.TOUCH_START, () => {
            this.node.emit(KEYPAD_EVENT.PRESS_RIGHT);
        });

        this.keypadDown?.node.on(Node.EventType.TOUCH_START, () => {
            this.node.emit(KEYPAD_EVENT.PRESS_DOWN);
        });

        this.keypadLeft?.node.on(Node.EventType.TOUCH_START, () => {
            this.node.emit(KEYPAD_EVENT.PRESS_LEFT);
        });
    }
}
