import { _decorator, Component, Node, RichText } from 'cc';
import { BaseText } from '../lib/text/baseText';
const { ccclass, property } = _decorator;

@ccclass('ScoreHeader')
export class ScoreHeader extends Component {
    @property(BaseText)
    public readonly scoreText?: BaseText;

    @property(BaseText)
    public readonly highscoreText?: BaseText;
    
    public show() {
        this.node.active = true;
    }

    public hide() {
        this.node.active = false;
    }

    public updateScore(score: number) {
        this.scoreText?.setText(Math.round(score).toString());
    }

    public updateHighscore(highscore: number) {
        this.highscoreText?.setText(Math.round(highscore).toString());
    }
}
