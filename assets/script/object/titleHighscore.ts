
import { _decorator, Component, Node } from 'cc';
import { BaseText } from '../lib/text/baseText';
import { getHighscoreFromLocalStorage } from '../lib/util/localStorage';
const { ccclass, property } = _decorator;

@ccclass('TitleHighscore')
export class TitleHighscore extends Component {
    @property(BaseText)
    public readonly highscoreText?: BaseText;

    start() {
        this.highscoreText?.setText(getHighscoreFromLocalStorage().toString());
    }
}
