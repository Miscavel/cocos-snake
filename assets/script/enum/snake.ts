export enum SNAKE_BODY_PART {
  HEAD = 'head',
  BODY = 'body',
  BODY_FAT = 'body_fat',
  TAIL = 'tail',
}

export enum SNAKE_EVENT {
  MOVE = 'move',
}