export enum GAME_CONTROL_EVENT {
  GAME_OVER = 'game_over',
  CHANGE_SNAKE_DIRECTION = 'change_snake_direction',
  EAT_FRUIT = 'eat_fruit',
}