export enum KEYPAD_EVENT {
  PRESS_UP = 'press_up',
  PRESS_RIGHT = 'press_right',
  PRESS_DOWN = 'press_down',
  PRESS_LEFT = 'press_left',
}