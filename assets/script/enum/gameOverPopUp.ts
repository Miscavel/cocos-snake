export enum GAME_OVER_POP_UP_EVENT {
  CANCEL = 'cancel',
  PLAY_AGAIN = 'play_again',
}