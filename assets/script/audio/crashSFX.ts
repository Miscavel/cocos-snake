import { _decorator, Component, Node } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseAudio } from './baseAudio';
const { ccclass, property } = _decorator;

@ccclass('CrashSFX')
export class CrashSFX extends BaseAudio {
    constructor() {
        super('CrashSFX', ASSET_KEY.CRASH_SFX, false, 0.6);
    }
}
