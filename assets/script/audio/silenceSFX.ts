import { _decorator, Component, Node } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseAudio } from './baseAudio';
const { ccclass, property } = _decorator;

@ccclass('SilenceSFX')
export class SilenceSFX extends BaseAudio {
    constructor() {
        super('SilenceSFX', ASSET_KEY.SILENCE_SFX);
    }
}