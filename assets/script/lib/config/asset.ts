import { ASSET_EXTENSION, ASSET_KEY, ASSET_TYPE } from "../enum/asset";
import { AssetConfig } from "../interface/asset";

function getShopeeAssetUrl(url: string) {
    return `https://cf.shopee.co.id/file/${url}`;
}

export function getAssets() {
    const assets = new Array<AssetConfig>();

    // Early sprites (used for loading screen)
    assets.push({
        key: ASSET_KEY.WHITE_BOX_SPRITE,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/white_box'
    });
    assets.push({
        key: ASSET_KEY.LOGO_SPRITE,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/logo_shopee_ular'
    });

    // Font
    assets.push({
        key: ASSET_KEY.SHOPEE_2021_BOLD,
        type: ASSET_TYPE.FONT,
        url: '',
        localUrl: 'font/Shopee2021/Shopee2021-Bold'
    });
    assets.push({
        key: ASSET_KEY.SHOPEE_2021_MEDIUM,
        type: ASSET_TYPE.FONT,
        url: '',
        localUrl: 'font/Shopee2021/Shopee2021-Medium'
    });

    // Sprite
    assets.push({
        key: ASSET_KEY.APPLE_SPRITE,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/sprite_apple'
    });
    assets.push({
        key: ASSET_KEY.SOUND_ON_SPRITE,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/sprite_sound_on'
    });
    assets.push({
        key: ASSET_KEY.SOUND_OFF_SPRITE,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/sprite_sound_off'
    });
    assets.push({
        key: ASSET_KEY.TROPHY_SPRITE,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/sprite_trophy'
    });
    assets.push({
        key: ASSET_KEY.WALL_SPRITE,
        type: ASSET_TYPE.IMAGE,
        url: '',
        localUrl: 'image/sprite_wall'
    });
    
    // Spritesheet
    assets.push({
        key: ASSET_KEY.KEYPAD_SPRITESHEET,
        type: ASSET_TYPE.SPRITESHEET,
        url: '',
        localUrl: 'image/keypad',
        config: {
            frameWidth: 124,
            frameHeight: 124,
            paddingX: 20,
            paddingY: 16
        }
    });
    assets.push({
        key: ASSET_KEY.TILE_SPRITESHEET,
        type: ASSET_TYPE.SPRITESHEET,
        url: '',
        localUrl: 'image/sprite_tile',
        config: {
            frameWidth: 48,
            frameHeight: 48
        }
    });
    assets.push({
        key: ASSET_KEY.SNAKE_SPRITESHEET,
        type: ASSET_TYPE.SPRITESHEET,
        url: '',
        localUrl: 'image/spritesheet_round',
        config: {
            frameWidth: 96,
            frameHeight: 96,
            paddingX: 1
        }
    });
    
    // Soundtrack
    assets.push({
        key: ASSET_KEY.BACKGROUND_SOUNDTRACK,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/bg-music'
    });

    // SFX
    assets.push({
        key: ASSET_KEY.EAT_SFX,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/eat'
    });
    assets.push({
        key: ASSET_KEY.TURN_SFX,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/turn'
    });
    assets.push({
        key: ASSET_KEY.CRASH_SFX,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/crash'
    });
    assets.push({
        key: ASSET_KEY.SILENCE_SFX,
        type: ASSET_TYPE.AUDIO,
        url: '',
        localUrl: 'audio/silence'
    })

    return assets;
}