import { _decorator, Component, Node, RichText } from 'cc';
import { LoadingBar } from './loadingBar';
const { ccclass, property } = _decorator;

@ccclass('AssetLoadingUI')
export class AssetLoadingUI extends Component {
    @property(RichText)
    public percentLoadText?: RichText;

    @property(RichText)
    public urlLoadText?: RichText;

    @property(LoadingBar)
    public readonly loadingBar?: LoadingBar; 

    public updateText(progress: number, key?: string) {
        const { percentLoadText, urlLoadText } = this;
        const progressPercent = Math.floor(progress * 100);
        
        this.loadingBar?.drawInnerGraphics(progressPercent);

        if (percentLoadText) {
            percentLoadText.string = `${progressPercent}%`;
        }
        
        if (urlLoadText) {
            switch(progressPercent) {
                case 100: {
                    urlLoadText.string = 'CLICK TO ENTER';
                    break;
                }

                case 0: {
                    urlLoadText.string = 'LOADING...';
                    break;
                }

                default: {
                    urlLoadText.string = `${key}`;
                    break;
                }
            }
        }
    }
}
