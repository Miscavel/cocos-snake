export interface BoardConfig {
  tiles: Array<Array<number>>;
}