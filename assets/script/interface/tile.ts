import { math, Node } from "cc";

export interface Tile {
  value: number;
  node?: Node;
  index: math.Vec2;
}