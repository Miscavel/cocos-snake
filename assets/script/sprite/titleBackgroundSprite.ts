
import { _decorator, Component, Node, Color } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseSprite } from '../lib/sprite/baseSprite';
const { ccclass, property } = _decorator;

@ccclass('TitleBackgroundSprite')
export class TitleBackgroundSprite extends BaseSprite {
    private readonly bgColor = new Color(26, 51, 98);
    
    constructor() {
        super('TitleBackgroundSprite', ASSET_KEY.WHITE_BOX_SPRITE);
    }

    onLoad() {
        super.onLoad();
        this.setColor(this.bgColor);
    }
}