
import { _decorator, Component, Node } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseSprite } from '../lib/sprite/baseSprite';
const { ccclass, property } = _decorator;

@ccclass('SoundButtonSprite')
export class SoundButtonSprite extends BaseSprite {
    private readonly soundButtonOnKey = ASSET_KEY.SOUND_ON_SPRITE;

    private readonly soundButtonOffKey = ASSET_KEY.SOUND_OFF_SPRITE;

    constructor() {
        super('SoundButtonSprite', ASSET_KEY.SOUND_ON_SPRITE);
    }

    public setOn() {
        this.setTexture(this.soundButtonOnKey);
        this.reload();
    }

    public setOff() {
        this.setTexture(this.soundButtonOffKey);
        this.reload();
    }
}
