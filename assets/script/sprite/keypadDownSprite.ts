import { _decorator, Component, Node } from 'cc';
import { ASSET_KEY } from '../lib/enum/asset';
import { BaseSprite } from '../lib/sprite/baseSprite';
const { ccclass, property } = _decorator;

@ccclass('KeypadDownSprite')
export class KeypadDownSprite extends BaseSprite {
    constructor() {
        super('KeypadDownSprite', ASSET_KEY.KEYPAD_SPRITESHEET, 4);
    }
}